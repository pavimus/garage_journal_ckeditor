export COMPOSE_PROJECT_NAME=$(basename $(dirname $(pwd)))

export COMPOSE_SYS_PROJECT_NAME=`echo "$COMPOSE_PROJECT_NAME" | sed "s/[^-_0-9a-z]//g"`

export PROJECT_DIR=$(dirname $(pwd))

export DOCKER_COMPOSE_ARGS="-f docker-compose.yml "

if [[ "$OSTYPE" == "darwin"* ]]; then
    export DOCKER_COMPOSE_ARGS="$DOCKER_COMPOSE_ARGS -f docker-compose.macos.yml "
fi

function get_container_id {
    docker ps | grep ${COMPOSE_SYS_PROJECT_NAME}_$1 | awk '{print $1}'
}

function get_container_env {
    docker exec -ti $CONTAINER_ID /bin/sh -c "cat /proc/1/environ" | xargs -0 -I % echo "export %;" | sed 's/\=\(.*\);/="\1";/g' | grep -v "HOME="
}
