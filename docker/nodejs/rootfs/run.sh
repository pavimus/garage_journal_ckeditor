#!/bin/bash

got_sigkill=0

_term() { 
  echo "Caught SIGTERM signal!" 
  kill -KILL "$child" 2>/dev/null
  got_sigkill=1
}

trap _term INT TERM

cd /var/www/${COMPOSE_PROJECT_NAME}

sudo -u node npm install &

child=$! 
wait "$child"
if [ "$got_sigkill" -eq "1" ]; then
   exit;
fi

sudo -u node yarn run build &

child=$! 
wait "$child"
if [ "$got_sigkill" -eq "1" ]; then
   exit;
fi

sleep 3600
